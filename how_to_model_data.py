
# coding: utf-8

# ## Import & Cleaning

# In[2]:


import pandas as pd

data_df = pd.DataFrame(pd.read_csv('PRSA_data_2010.1.1-2014.12.31.csv', index_col = 0))
data_df.head()


# In[2]:


data_df['datetime'] = pd.to_datetime(data_df[['year', 'month','day', 'hour']])
data_df = data_df.drop(['year', 'month','day', 'hour'], axis = 1)

data_df = data_df[['datetime', 'DEWP', 'TEMP', 'PRES', 'cbwd', 'Iws', 'Is', 'Ir', 'pm2.5']].iloc[24:].reset_index().drop('No', axis = 1)
data_df[['pm2.5']] = data_df[['pm2.5']].interpolate(method='linear')


# In[3]:


data_df.isnull().sum()


# In[4]:


len(data_df)


# In[5]:


ax = data_df['pm2.5'].plot.hist(bins = 200, figsize = (12,8))


# In[6]:


ax = data_df.plot(x = 'datetime', y = 'pm2.5', figsize = (12,8))


# In[7]:


data_df[['pm2.5']].mean()


# ## Simple Model

# In[8]:


from sklearn.model_selection import train_test_split

data_df = data_df.set_index('datetime')
X_train, X_test, y_train, y_test = train_test_split(data_df[['DEWP', 'TEMP', 'PRES', 'Iws', 'Is', 'Ir']], data_df[['pm2.5']], test_size=0.2)


# In[3]:


import statsmodels.api as sm
import numpy as np

model = sm.OLS(np.asarray(y_train), np.asarray(X_train))
results = model.fit()
print(results.summary())


# In[10]:


from sklearn.metrics import mean_squared_error

y_pred = results.predict(np.asarray(X_test))
mse = mean_squared_error(np.asarray(y_test), y_pred)
rmse = mse**0.5
print(rmse)


# ## Feature Engineering

# In[11]:


data_df = data_df.reset_index()
six_df = pd.DataFrame()
twelve_df = pd.DataFrame()
tf_df = pd.DataFrame()
for i in range(len(data_df)):
    six_df = pd.concat([six_df,pd.DataFrame({'6hr_avg':[sum(data_df['pm2.5'].iloc[i:i+6])/6]}, index = [i+6])])
    twelve_df = pd.concat([twelve_df,pd.DataFrame({'12hr_avg':[sum(data_df['pm2.5'].iloc[i:i+12])/12]}, index = [i+12])])
    tf_df = pd.concat([tf_df,pd.DataFrame({'24hr_avg':[sum(data_df['pm2.5'].iloc[i:i+24])/24]}, index = [i+24])])
    
data_df = pd.merge(data_df, six_df, left_index=True, right_index=True)
data_df = pd.merge(data_df, twelve_df, left_index=True, right_index=True)
data_df = pd.merge(data_df, tf_df, left_index=True, right_index=True)

data_df = pd.merge(data_df,pd.get_dummies(data_df['cbwd']), left_index=True, right_index=True)


# In[103]:


X_train, X_test, y_train, y_test = train_test_split(data_df[['DEWP', 'TEMP', 'PRES', 'Iws', 'Is', 'Ir','6hr_avg', '12hr_avg', '24hr_avg','NE','NW','SE']], data_df[['pm2.5']], test_size=0.2)

model = sm.OLS(np.asarray(y_train), np.asarray(X_train))
results = model.fit()
print(results.summary())


# In[104]:


y_pred = results.predict(np.asarray(X_test))

mse = mean_squared_error(np.asarray(y_test), y_pred)
rmse = mse**0.5
print(rmse)


# In[14]:


comp_df = pd.merge(y_test,pd.DataFrame({'Pred':y_pred}, index = [y_test.index]), left_index=True, right_index=True)
ax = comp_df.plot.hist(bins= 200, alpha = 0.5, figsize=(12,8))


# ## A Better Model

# In[25]:


from sklearn import tree


# In[33]:


data_df = data_df.reset_index()
X_train, X_test, y_train, y_test = train_test_split(data_df[['DEWP', 'TEMP', 'PRES', 'Iws', 'Is', 'Ir','6hr_avg', '12hr_avg', '24hr_avg','NE','NW','SE']], data_df[['pm2.5']], test_size=0.2)


# In[27]:


clf = tree.DecisionTreeRegressor()
clf = clf.fit(np.asarray(X_train), np.asarray(y_train))


# In[28]:


y_pred = clf.predict(np.asarray(X_test))


# In[29]:


mse = mean_squared_error(np.asarray(y_test), y_pred)
rmse = mse**0.5
print(rmse)


# In[101]:


import xgboost as xgb
from sklearn.metrics import accuracy_score

dtest = xgb.DMatrix(X_test, y_test, feature_names=X_test.columns)
dtrain = xgb.DMatrix(X_train, y_train,feature_names=X_train.columns)

param = {'verbosity':1, 
         'objective':'reg:squarederror',     ####'reg:squarederror', 
         'booster':'gblinear',
         'eval_metric' :'rmse',
         'feature_selector': 'cyclic',
         'learning_rate': 1}

evallist = [(dtrain, 'train')]


# In[102]:


num_round = 20
bst = xgb.train(param, dtrain, num_round, evallist)


# In[119]:


import xgboost as xgb

dtest = xgb.DMatrix(X_test, y_test, feature_names=X_test.columns)
dtrain = xgb.DMatrix(X_train, y_train,feature_names=X_train.columns)

param = {'verbosity':1, 
         'objective':'reg:squarederror',
         'booster':'gblinear',
         'eval_metric' :'rmse',
         'learning_rate': 1}

evallist = [(dtrain, 'train')]


# In[120]:


num_round = 20
bst = xgb.train(param, dtrain, num_round, evallist)


# In[107]:


y_pred = bst.predict(dtest)


# In[108]:


mse = mean_squared_error(np.asarray(y_test), y_pred)
rmse = mse**0.5
print(rmse)


# In[4]:





# In[6]:




